package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ButtonTextViewEditTextActivity extends AppCompatActivity {

    Button btn1, faktoriyel;
    TextView textView1, faktoriyelSonuc;
    EditText editText1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        define();
        pressButton();
        changeText();
        faktoritelHesapla();

    }


    private void define() {

        btn1 = findViewById(R.id.button1);
        textView1 = findViewById(R.id.textView1);


        editText1 = findViewById(R.id.editText1);
        faktoriyel = findViewById(R.id.faktoriyelButton);
        faktoriyelSonuc = findViewById(R.id.faktoritelSonuc);

    }


    public void pressButton() {


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked the buton 1......");
            }
        });
    }

    public void changeText() {

        textView1.setText("faruk akyol");

    }

    public void faktoritelHesapla() {

        faktoriyel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String deger = editText1.getText().toString();
                int fak = Integer.parseInt(deger);
                long sonuc = factorial(fak);

                String sonuc2 = String.valueOf(sonuc);


                faktoriyelSonuc.setText("Faktoriyel sonuc : " + sonuc2);
            }
        });


    }


    public static long factorial(int number) {
        long result = 1;

        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        return result;
    }
}

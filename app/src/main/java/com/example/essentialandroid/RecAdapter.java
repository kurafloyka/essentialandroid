package com.example.essentialandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecAdapter extends RecyclerView.Adapter<RecAdapter.tanimla> {

    Context context;
    List<MesajModel2> list;

    public RecAdapter(Context context, List<MesajModel2> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public tanimla onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.kullanici3, parent, false);
        return new tanimla(view);
    }

    @Override
    public void onBindViewHolder(@NonNull tanimla holder, final int position) {

        holder.mesaj.setText(list.get(position).getMesaj());
        holder.kisi.setText(list.get(position).getIsim());
        holder.img.setImageResource(list.get(position).getResimId());

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, list.get(position).getIsim(), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class tanimla extends RecyclerView.ViewHolder {

        ImageView img;
        TextView kisi;
        TextView mesaj;

        public tanimla(@NonNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.resim);
            kisi = itemView.findViewById(R.id.kisi);
            mesaj = itemView.findViewById(R.id.mesaj);

        }
    }


}

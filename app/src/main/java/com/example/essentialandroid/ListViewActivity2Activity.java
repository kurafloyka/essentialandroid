package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity2Activity extends AppCompatActivity {


    ListView listView;
    List<MesajModel> liste;
    MesajAdapter adp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_activity2);
        define();
        addValues();
    }


    public void define() {

        listView = findViewById(R.id.listview);


    }

    public void addValues() {
        liste = new ArrayList<>();
        MesajModel m1 = new MesajModel("Merhaba Ben Niloya", "Niloya", R.drawable.turkey);
        MesajModel m2 = new MesajModel("Merhaba ben Adana", "Adana", R.drawable.comment);
        MesajModel m3 = new MesajModel("Merhaba Ben Istanbul", "Istanbul", R.drawable.like);

        liste.add(m1);
        liste.add(m2);
        liste.add(m3);
        adp = new MesajAdapter(liste, this);
        listView.setAdapter(adp);


    }
}

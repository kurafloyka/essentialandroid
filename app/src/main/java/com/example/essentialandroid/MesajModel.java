package com.example.essentialandroid;

public class MesajModel {

    private String icerik;
    private String kisi;
    private int resimId;

    public MesajModel(String icerik, String kisi, int resimId) {
        this.icerik = icerik;
        this.kisi = kisi;
        this.resimId = resimId;
    }


    public String getIcerik() {
        return icerik;
    }

    public void setIcerik(String icerik) {
        this.icerik = icerik;
    }

    public String getKisi() {
        return kisi;
    }

    public void setKisi(String kisi) {
        this.kisi = kisi;
    }

    public int getResimId() {
        return resimId;
    }

    public void setResimId(int resimId) {
        this.resimId = resimId;
    }
}

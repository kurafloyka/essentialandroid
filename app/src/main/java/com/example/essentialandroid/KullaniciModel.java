package com.example.essentialandroid;

public class KullaniciModel {

    String isim;
    String yas;
    String soyIsim;
    String tuttuguTakim;


    public KullaniciModel(String isim, String yas, String soyIsim, String tuttuguTakim) {
        this.isim = isim;
        this.yas = yas;
        this.soyIsim = soyIsim;
        this.tuttuguTakim = tuttuguTakim;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public String getYas() {
        return yas;
    }

    public void setYas(String yas) {
        this.yas = yas;
    }

    public String getSoyIsim() {
        return soyIsim;
    }

    public void setSoyIsim(String soyIsim) {
        this.soyIsim = soyIsim;
    }

    public String getTuttuguTakim() {
        return tuttuguTakim;
    }

    public void setTuttuguTakim(String tuttuguTakim) {
        this.tuttuguTakim = tuttuguTakim;
    }
}

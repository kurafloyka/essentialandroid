package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class CheckBoxRadioButtonActivity extends AppCompatActivity {

    CheckBox php, js, java, csharp, python;
    RadioButton img1, img2, img3, img4;
    ImageView imageView;
    RadioGroup radioGroup;
    Button degistirButon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box_radio_button);

        define();
        showMessage();
        //selectRadioButton();
        changeImage();
    }

    public void define() {

        php = findViewById(R.id.php);
        js = findViewById(R.id.js);
        java = findViewById(R.id.java);
        csharp = findViewById(R.id.csharp);
        python = findViewById(R.id.python);

        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);

        imageView = findViewById(R.id.imageView);
        radioGroup = findViewById(R.id.radioGroup);
        degistirButon = findViewById(R.id.degistirButon);


    }

    public void showMessage() {

        java.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println(java.isChecked());
                if (java.isChecked()) {

                    Toast.makeText(getApplicationContext(), java.getText() + " secildi", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getApplicationContext(), java.getText() + " iptal edildi", Toast.LENGTH_LONG).show();
                }
            }
        });

        php.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println(php.isChecked());
                if (php.isChecked()) {

                    Toast.makeText(getApplicationContext(), php.getText() + " secildi", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getApplicationContext(), php.getText() + " iptal edildi", Toast.LENGTH_LONG).show();
                }
            }
        });


    }


    public void selectRadioButton() {
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageResource(R.drawable.share);
                Toast.makeText(getApplicationContext(), img1.getText(), Toast.LENGTH_SHORT).show();
            }
        });


        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageResource(R.drawable.turkey);
                Toast.makeText(getApplicationContext(), img2.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageResource(R.drawable.comment);
                Toast.makeText(getApplicationContext(), img3.getText(), Toast.LENGTH_SHORT).show();
            }
        });


        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageResource(R.drawable.like);
                Toast.makeText(getApplicationContext(), img4.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getId() {
        int id = radioGroup.getCheckedRadioButtonId();
        resimDegis(id);

    }

    private void resimDegis(int id) {


        if (id == R.id.img1) {
            imageView.setImageResource(R.drawable.share);
            Toast.makeText(getApplicationContext(), img1.getText(), Toast.LENGTH_SHORT).show();

        } else if (id == R.id.img2) {
            imageView.setImageResource(R.drawable.turkey);
            Toast.makeText(getApplicationContext(), img2.getText(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.img3) {

            imageView.setImageResource(R.drawable.comment);
            Toast.makeText(getApplicationContext(), img3.getText(), Toast.LENGTH_SHORT).show();

        } else {
            imageView.setImageResource(R.drawable.like);
            Toast.makeText(getApplicationContext(), img4.getText(), Toast.LENGTH_SHORT).show();
        }


    }


    public  void changeImage(){

        degistirButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getId();
            }
        });
    }

}


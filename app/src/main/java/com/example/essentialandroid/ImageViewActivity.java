package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageViewActivity extends AppCompatActivity {


    ImageView imageView;
    Button changeImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);


        define();
        clickChangeButton();
        Toast.makeText(getApplicationContext(), "FARUK", Toast.LENGTH_SHORT).show();

    }


    private void define() {


        imageView = findViewById(R.id.imageView2);
        changeImage = findViewById(R.id.changePicture);

    }


    public void clickChangeButton() {

        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int rnd = (int) (Math.random() * 5 + 1);
                System.out.println(rnd);


                changePicture(rnd);
                Toast.makeText(getApplicationContext(), String.valueOf(rnd) + " numarali Image secildi....", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void changePicture(int deger) {


        if (deger == 1) {
            imageView.setImageResource(R.drawable.turkey);
        } else if (deger == 2) {
            imageView.setImageResource(R.drawable.like);
        } else if (deger == 3) {
            imageView.setImageResource(R.drawable.comment);
        } else if (deger == 4) {
            imageView.setImageResource(R.drawable.share);
        } else {
            imageView.setImageResource(R.drawable.password);
        }


    }



}

package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Adapter;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity {

    List<MesajModel2> liste;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecAdapter recAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        tanimla();
        listeDoldur();
    }

    public void tanimla() {

        recyclerView = findViewById(R.id.recy);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void listeDoldur() {

        liste = new ArrayList<>();
        MesajModel2 m1 = new MesajModel2("Merhaba Ben AHMET", "Niloya", R.drawable.turkey);
        MesajModel2 m2 = new MesajModel2("Merhaba ben MEHMET", "Adana", R.drawable.comment);
        MesajModel2 m3 = new MesajModel2("Merhaba Ben MUSTAFA", "Istanbul", R.drawable.like);

        liste.add(m1);
        liste.add(m2);
        liste.add(m3);

        recAdapter = new RecAdapter(this, liste);
        recyclerView.setAdapter(recAdapter);

    }
}

package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    List<KullaniciModel> kullaniciList;
    KullaniciListAdapter adp;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        define();
        listeDoldur();
    }


    public void listeDoldur() {
        kullaniciList = new ArrayList<>();
        KullaniciModel k1 = new KullaniciModel("faruk", "28", "akyol", "GS");
        KullaniciModel k2 = new KullaniciModel("fatih", "33", "akyol", "bjk");
        KullaniciModel k3 = new KullaniciModel("elif", "55", "akyol", "fb");
        KullaniciModel k4 = new KullaniciModel("ahmet", "61", "akyol", "GS");

        kullaniciList.add(k1);
        kullaniciList.add(k2);
        kullaniciList.add(k3);
        kullaniciList.add(k4);
        adp = new KullaniciListAdapter(kullaniciList, this);
        listView.setAdapter(adp);

    }

    public void define(){

        listView = findViewById(R.id.listview);
    }
}

package com.example.essentialandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class KullaniciListAdapter extends BaseAdapter {

    List<KullaniciModel> list;
    Context context;

    public KullaniciListAdapter(List<KullaniciModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View layout = LayoutInflater.from(context).inflate(R.layout.kullanici, parent, false);

        TextView isim = layout.findViewById(R.id.isim);
        TextView soyisim = layout.findViewById(R.id.soyisim);
        TextView yas = layout.findViewById(R.id.yas);
        TextView takim = layout.findViewById(R.id.takim);
        isim.setText(list.get(position).getIsim());
        soyisim.setText(list.get(position).getSoyIsim());
        yas.setText(list.get(position).getYas());
        takim.setText(list.get(position).getTuttuguTakim());
        return layout;
    }
}

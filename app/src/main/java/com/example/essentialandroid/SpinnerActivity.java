package com.example.essentialandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class SpinnerActivity extends AppCompatActivity {


    Spinner spinner;
    String[] ilceler = {"Beykoz", "Adalar", "Kadikoy", "Umraniye"};
    ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        tanimla();
        createAdapter();
        setSpinnerAdapter();
        bilgiVer();
    }

    public void tanimla() {

        spinner = findViewById(R.id.spinner);
    }


    public void createAdapter() {

        arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, ilceler);
    }

    public void setSpinnerAdapter() {
        spinner.setAdapter(arrayAdapter);
    }

    public void bilgiVer() {

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), "" + spinner.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
